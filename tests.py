import argparse
import os
import subprocess

app = 'bld/itseez_interview'

tests = [
	{ 'data' : ['', 0],        'tags' : ['simple', 'empty']           },
	{ 'data' : [' + 4', 4],    'tags' : ['simple', 'one_number']      },
	{ 'data' : [' - 3', -3],   'tags' : ['simple', 'one_number']      },
	{ 'data' : ['2456', 2456], 'tags' : ['simple', 'one_number']      },
	{ 'data' : ['2 + 2', 4],   'tags' : ['simple', 'addition']        },
	{ 'data' : ['3 - 2', 1],   'tags' : ['simple', 'subtraction']     },
	{ 'data' : ['2 * 5', 10],  'tags' : ['simple', 'multiplication']  },
	{ 'data' : ['15 / 3', 5],  'tags' : ['simple', 'division']  },

	{ 'data' : ['2 + 2 * 2', 6],  'tags' : ['priority', 'addition', 'multiplication']  },
	{ 'data' : ['2 * 2 + 2', 6],  'tags' : ['priority', 'addition', 'multiplication']  },
	{ 'data' : ['4 / 2 * 3', 6],  'tags' : ['priority', 'division', 'multiplication']  },
	{ 'data' : ['(2 + 2) * 3', 12],  'tags' : ['priority', 'addition']  },
	{ 'data' : ['(((2 + 2) * 2 + 2) * 2 + 2 ) * 2', 44],  'tags' : ['priority', 'addition']  },
	{ 'data' : ['(1 + ( 1 + (2 + 2) * 2 + 2) * 2 + 2 ) * 2', 50],  'tags' : ['priority', 'addition']  },
	{ 'data' : ['(2 + 2) * (2+5*2) / 2', 24],  'tags' : ['priority', 'addition', 'division', 'multiplication']  },
	{ 'data' : ['((2 + 2) * 3 + 4) / 2', 8],  'tags' : ['priority', 'addition', 'division', 'multiplication']  },
	{ 'data' : ['( + 4)', 4],    'tags' : ['simple', 'one_number']  }

]

def get_all_tags():
	all_tags = []
	for test in tests:
		test['tags'].append('all') # to run all tests
		all_tags += test['tags'] 

	return set(all_tags)

all_tags = get_all_tags()
print "Tags:", all_tags

parser = argparse.ArgumentParser(description='Parser for exp parser test system')

parser.add_argument('--tags', choices=all_tags, nargs='+')

args = parser.parse_args()
if not args.tags:
	args.tags = 'all'

print "Types of tests to run: ", args.tags

total  = len(tests)
run    = 0
passed = 0
failed = 0
for test in tests:
	intersection = set(test['tags']) & set(args.tags);
	if not intersection:
		continue

	run += 1
	in_data, out_data = test['data']
	p = subprocess.Popen(app, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
	p.stdin.write(in_data)
	output, err = p.communicate()

	for line in output.split(os.linesep):
		if 'Result: ' in line:
			res = int(line.split('Result: ')[1])

			if res == out_data:
				print "\tPassed",
				passed += 1
			else:
				print "\tFailed",
				failed += 1

	print "'" + in_data + "'", " = ", out_data

print "Total:", total, "\tRun:", run, "\tPassed:", passed, "\tFailed:", failed