#include <iostream>
#include <string>
#include <map>

using namespace std;

///
enum enTokenType
{
    BEGIN,          ///< Meta-token - start of the program
    NUMBER,         ///< Any integer number
    PLUS,           ///< Plus sign
    MINUS,          ///< Minus sign
    MULTIYPLY,      ///< Multiplication sign
    DIVISION,       ///< Division sign
    LEFT_BRACKET,   ///< Left bracket
    RIGHT_BRACKET,  ///< Right bracket
    END             ///< Meta-token - end of the program
};

///
struct Token
{
    enTokenType type;   ///< Type of the token
    string value;       ///< Value of the token
    int startPos;       ///< Start position in the input stream
    int endPos;         ///< End position in the input stream
    Token *prev;        ///< Reference to the previous token
    Token *next;        ///< Reference to the next token
};

class Lexer
{
    map<enTokenType, string> tokenNames;
    int currentPosition;
    int len;
    string str;

    Token* tokenChain;

    void readNumber();

public:
    Lexer(const string& inputStr);

    void parse(const string& inputStr);

    Token* getFirstToken();
};

Lexer::Lexer(const string& inputStr)
{
    tokenNames[BEGIN]          = "BEGIN";
    tokenNames[NUMBER]         = "NUMBER";
    tokenNames[PLUS]           = "PLUS";
    tokenNames[MINUS]          = "MINUS";
    tokenNames[MULTIYPLY]      = "MULTIYPLY";
    tokenNames[DIVISION]       = "DIVISION";
    tokenNames[LEFT_BRACKET]   = "LEFT_BRACKET";
    tokenNames[RIGHT_BRACKET]  = "RIGHT_BRACKET";
    tokenNames[END]            = "END";

    parse(inputStr);
}

void Lexer::parse(const string& inputStr)
{
    tokenChain      = NULL;
    currentPosition = 0;
    len = inputStr.length();
    str = inputStr;
    cout << "Expression to parse: " << str << endl;

    Token *prevToken = new Token;
    prevToken->type     = BEGIN;
    prevToken->startPos = 0;
    prevToken->endPos   = 0;
    prevToken->value    = "begin";
    prevToken->prev     = NULL;

    tokenChain = prevToken;

    cout << "Lexer output:" << endl;

    while(currentPosition < len)
    {
        if(isspace(str[currentPosition]))
        {
            currentPosition++;
            continue;
        }

        Token *token = new Token;
        token->startPos = currentPosition;

        if(isnumber(str[currentPosition]))
        {
            token->type     = NUMBER;
            readNumber();
            token->endPos = currentPosition;
            token->value  = str.substr(token->startPos, token->endPos - token->startPos + 1);
        }
        else if(str[currentPosition] == '+')
        {
            token->type     = PLUS;
            token->endPos   = currentPosition + 1;
            token->value    = "+";
        }
        else if(str[currentPosition] == '-')
        {
            token->type     = MINUS;
            token->endPos   = currentPosition + 1;
            token->value    = "-";
        }
        else if(str[currentPosition] == '*')
        {
            token->type     = MULTIYPLY;
            token->endPos   = currentPosition + 1;
            token->value    = "*";
        }
        else if(str[currentPosition] == '/')
        {
            token->type     = DIVISION;
            token->endPos   = currentPosition + 1;
            token->value    = "/";
        }
        else if(str[currentPosition] == '(')
        {
            token->type     = LEFT_BRACKET;
            token->endPos   = currentPosition + 1;
            token->value    = "(";
        }
        else if(str[currentPosition] == ')')
        {
            token->type     = RIGHT_BRACKET;
            token->endPos   = currentPosition + 1;
            token->value    = ")";
        }
        else
            throw string("Parse error, unexpected symbol");

        token->prev     = prevToken;
        prevToken->next = token;
        cout << "\tType: " << tokenNames[token->type] << "\t\tvalue:" << token->value << endl;

        prevToken = token;

        currentPosition++;
    }

    Token *token = new Token;
    token->startPos = len - 1;
    token->endPos   = len - 1;
    token->type     = END;
    token->value    = "end";
    token->next     = NULL;
    token->prev     = prevToken;

    prevToken->next = token;
}

Token* Lexer::getFirstToken()
{
    return tokenChain;
}

void Lexer::readNumber()
{
    while(currentPosition < len && isnumber((str[currentPosition])))
    {
        currentPosition++;
    }
    currentPosition--;
}

////////////////////////////////////////////////////////////////////
class Syntaxer
{
    Lexer *lexer;

    int depth;

    int calculate(Token *startToken, int leftValue = 0);

    int getOperand(Token* startToken);

    int calculateExp(Token *startToken);

    Token *currentToken;

public:
    Syntaxer(Lexer *lex);

    void parse(Lexer *lex);
};

Syntaxer::Syntaxer(Lexer *lex)
{
    parse(lex);
}

int Syntaxer::calculateExp(Token *startToken)
{
    currentToken = startToken;
    int res = 0;
    while(currentToken->type != END && currentToken->type != RIGHT_BRACKET)
    {
        res = calculate(currentToken, res);
    }
    return calculate(currentToken->next, res);
}

int Syntaxer::getOperand(Token* startToken)
{
    currentToken = startToken->next;
    if(startToken->type == NUMBER)
    {
        return atoi(startToken->value.c_str());
    }
    else if(startToken->type == LEFT_BRACKET)
    {
        int res = calculateExp(startToken->next);
        cout << "\tExpression:";
        Token *tmp = startToken;
        while(tmp != currentToken->next)
        {
            cout << tmp->value;
            tmp = tmp->next;
        }
        cout << endl;
        return res;
    }
    else
        throw string("unexpected token. Should be NUMBER or LEFT_BRACKET");
}

int Syntaxer::calculate(Token *startToken, int leftValue)
{
    if(startToken == NULL)
        throw string("List of tokens is empty");

    currentToken = startToken;

    if(startToken->type == BEGIN)
        return calculate(startToken->next);
    else if(startToken->type == END)
        return leftValue;
    else if(startToken->type == LEFT_BRACKET)
        return calculateExp(startToken->next);
    else if(startToken->type == NUMBER)
    {
        int left = getOperand(startToken);
        if(startToken->next->type == END || startToken->type == RIGHT_BRACKET)
            return left;

        int res = calculate(startToken->next, left);
        return res;
    }
    else if(startToken->type == MULTIYPLY)
    {
        int right = getOperand(startToken->next);
        cout << "\tMUL(" << leftValue << ", " << right << ")" << endl;
        int res = leftValue * right;
        if(currentToken->type == MULTIYPLY || currentToken->type == DIVISION)
            res = calculate(currentToken, res);

        return res;
    }
    else if(startToken->type == DIVISION)
    {
        int right = getOperand(startToken->next);
        cout << "\tDIV(" << leftValue << ", " << right << ")" << endl;
        int res = leftValue / right;
        if(currentToken->type == MULTIYPLY || currentToken->type == DIVISION)
            res = calculate(currentToken, res);

        return res;
    }
    else if(startToken->type == PLUS)
    {
        int right = getOperand(startToken->next);
        if(currentToken->type == MULTIYPLY || currentToken->type == DIVISION)
            right = calculate(currentToken, right);

        cout << "\tPLUS(" << leftValue << ", " << right << ")" << endl;
        int res = leftValue + right;
        return res;
    }
    else if(startToken->type == MINUS)
    {
        int right = getOperand(startToken->next);
        if(currentToken->type == MULTIYPLY || currentToken->type == DIVISION)
            right = calculate(currentToken, right);

        cout << "\tMINUS(" << leftValue << ", " << right << ")" << endl;
        int res = leftValue - right;
        return res;
    }
    else if(startToken->type == LEFT_BRACKET)
    {
        return calculate(startToken->next);;
    }
    else if(startToken->type == RIGHT_BRACKET)
    {
        return leftValue;
    }
    else
        throw string("parse error");

    return 0;
}

void Syntaxer::parse(Lexer *lex)
{
    lexer = lex;
    currentToken = NULL;
    depth = 0;

    currentToken = lexer->getFirstToken();
    int res = 0;

    cout << "Syntaxer output:" <<endl;
    while(currentToken->type != END)
    {
        if(currentToken->type == RIGHT_BRACKET)
        {
            res = calculate(currentToken->next, res);
            continue;
        }

        res = calculate(currentToken, res);
    }

    cout << "Result: " << res << endl;
}

////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
    string inputStr;
    cout << "Please input your expression" << endl;

    std::getline(std::cin, inputStr);

    try
    {
        Lexer *lex = new Lexer(inputStr);

        Syntaxer synt(lex);
    }
    catch(const string& exception)
    {
        cout << "ERROR: " << exception << endl;
    }


    return 0;
}
